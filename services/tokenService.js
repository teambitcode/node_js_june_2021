var jwt = require('jsonwebtoken');
var authConfigs = require('../configs/auth-configs');

var key = authConfigs.encryptorSecretKey;

var encryptor = require('simple-encryptor')(key);


module.exports.createToken =  (userObject)=> {

    var userDetails = {
        email: userObject.email,
        first_name: userObject.first_name,
        last_name: userObject.last_name
    };

    var jwtToken = jwt.sign(userDetails, authConfigs.jwtKey,{ expiresIn: 30 });

    let encryptedJwtToken = encryptor.encrypt(jwtToken);

    return encryptedJwtToken;
}


module.exports.verifyToken = (request, response, nextFn) =>{

    console.log(request.headers.authorization);

    if (request.headers.authorization) {
        try {

            let decryptedToken = encryptor.decrypt(request.headers.authorization);


            var result = jwt.verify(decryptedToken, authConfigs.jwtKey);

            if(result){
                nextFn();
            }else {
                response.send("Token is invalid...");
            }
            
        } catch (error) {

            let isExp = error.hasOwnProperty('expiredAt')

            if(isExp){
                response.send("Token is expired...");
            }else{
                response.send("Token is invalid...");
            }

        }

    } else {
        response.send("Please loing and send again...");
    }

}