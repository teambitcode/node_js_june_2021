
module.exports.successWithData =  (response, data)=> {
    let res = {
        status: true,
        data: data
    }
    response.send(res);
}

module.exports.successWithToken =  (response, data)=> {
    let res = {
        status: true,
        message: data.message,
        token: data.token
    }
    response.send(res);
}


module.exports.errorWithMessage =  (response, message)=> {
    let res = {
        status: false,
        message: message
    }
    response.send(res);
}