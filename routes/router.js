
const express = require('express');
var router = express.Router();

const userSchema = require('../src/user/userSchema');
const productSchema = require('../src/product/productSchema');


const productController = require('../src/product/productController');
const userController = require('../src/user/user-controller');

const tokenService = require('../services/tokenService');


//product rotes
router.route('/product/getAll').get(tokenService.verifyToken, productController.getAllProducts);
router.route('/product/create').post(tokenService.verifyToken, productController.createProduct);
router.route('/product/update/:id').patch(productSchema.validatePatchBody, productController.updateProduct);
router.route('/product/delete/:id').delete(productController.deleteProduct);

//user routes
router.route('/user/create').post(userSchema.validatePostBody, userController.createUser);
router.route('/user/find').get(userController.getUsers);

router.route('/user/login').post(userController.loginUser);


module.exports = router;