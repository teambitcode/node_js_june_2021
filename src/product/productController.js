const responseService = require('../../services/response-service');
const producService = require('./product-service');



module.exports.getAllProducts = async (request, response) => {

    let myData = await producService.findAllProducts();
    if (myData == false) {

        responseService.errorWithMessage(response, "Error getting data");

    } else {

        responseService.successWithData(response, myData);
    }

}

module.exports.getAllProducts = async (request, response) => {

    let myData = await producService.findAllProducts();
    if (myData == false) {

        responseService.errorWithMessage(response, "Error getting data");

    } else {

        responseService.successWithData(response, myData);
    }

}



module.exports.createProduct = async (request, response) => {

    let myData = await producService.createProduct(request.body);

    if (myData == false) {
        responseService.errorWithMessage(response, "Error creating product");
    } else {
        responseService.successWithData(response, "Product created successuflly");
    }
}



module.exports.updateProduct = async (request, response) => {

    console.log(request.params.id);

    let myData = await producService.updateProduct(request.params.id, request.body);

    if (myData == false) {
        responseService.errorWithMessage(response, "Error creating product");
    } else {
        responseService.successWithData(response, myData);
    }
}

module.exports.deleteProduct = async (request, response) => {

    console.log(request.params.id);

    let myData = await producService.deleteProduct(request.params.id);

    if (myData == false) {
        responseService.errorWithMessage(response, "Error removing product");
    } else {
        responseService.successWithData(response, myData);
    }
}