const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const productSchema = new Schema({

    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    }

});

var prodModel = mongoose.model('products', productSchema);

module.exports = prodModel;