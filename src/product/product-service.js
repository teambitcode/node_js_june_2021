const productModel = require('./product-model');


module.exports.findAllProducts =  () =>{
   
    return new Promise(  ( resolve, reject ) =>{
 
        productModel.find({},  (error, result)=> {
    
            if (error) {
                reject(false);
            } else {
                resolve(result);
            }
    
        });
    
    } );

}

module.exports.createProduct =  (productDetail)=> {
   
    return new Promise( function promiseGetData( resolve, reject ) {

        var productModelData = new productModel();
        productModelData.description = productDetail.description;
        productModelData.price = productDetail.price;
 
        productModelData.save(function getResponse(error, result) {

            if (error) {
                reject(false);
            } else {
                resolve(true);
            }

        });
    
    } );

}


/**
 * 
 * @param {*} productId - product id that you want update
 * @param {*} productDetail - product detail object to update
 * @returns - false return with error and otherwise success
 */
module.exports.updateProduct =  (productId, productDetail)=> {
   
    return new Promise( function promiseGetData( resolve, reject ) {

        productModel.findByIdAndUpdate(productId, productDetail,  (error, result)=> {
    
            if (error) {
                reject(false);
            } else {
                resolve("Product updated successfully");
            }
    
        });

    
    } );

}

module.exports.deleteProduct =  (productId)=> {
   
    return new Promise( function promiseGetData( resolve, reject ) {

        productModel.findByIdAndRemove(productId,  (error, result)=> {
    
            if (error) {
                reject(false);
            } else {
                resolve("Product deleted successfully");
            }
    
        });

    
    } );

}