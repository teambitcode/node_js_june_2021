const Joi = require('joi');
const responseService = require('../../services/response-service');

var productPatchSchema = Joi.object().keys({
    description: Joi.string(),
    price: Joi.number()
});


module.exports.validatePatchBody = function name(req, res, next) {
    console.log("In validate fn");

    var result = productPatchSchema.validate(req.body);

    if (result.error) {
        responseService.errorWithMessage(res, result.error.message);
    } else {
        next();
    }
}