const userModel = require('./user-model');
const authConfigs = require('../../configs/auth-configs');
const tokenService = require('../../services/tokenService');

var key = authConfigs.encryptorSecretKey;

var encryptor = require('simple-encryptor')(key);


module.exports.createUser = (userData) => {

    return new Promise((resolve, reject) => {


        userModel.findOne({ email: userData.email }, function (err, userFindData) {

            if(err){
                reject(false);
            }
            else if(userFindData == undefined || userFindData == null)
            {
                var userModelData = new userModel();

                userModelData.first_name = userData.first_name;
                userModelData.last_name = userData.last_name;
                userModelData.email = userData.email;
        
                let encryptedPassword = encryptor.encrypt(userData.password);
        
                userModelData.password = encryptedPassword;
        
                userModelData.save(function getResponse(error, result) {
        
                    if (error) {
                        reject(false);
                    } else {
                        resolve(true);
                    }
        
                });

            }else{
                resolve(null);
            }

        });



    });

}


module.exports.loginUser = (userData) => {

    return new Promise((resolve, reject) => {

    
        userModel.findOne({ email: userData.email }, function getData(error, result) {

            if (error) {
                reject(false);
            } else {

                let decryptedPassword = encryptor.decrypt(result.password);

                if(decryptedPassword == userData.password){

                    let token = tokenService.createToken(result);

                    resolve({token: token, message :"User successfully looged"});
                }else{

                    reject(null);
                }

            }
    
        });



    });

}


module.exports.findUsers =  (condition) =>{
   
    return new Promise(  ( resolve, reject ) =>{
 
                        // condition =      {role: "admin"}
        userModel.find(condition,  (error, result)=> {
    
            if (error) {
                reject(false);
            } else {
                resolve(result);
            }
    
        });
    
    } );

}