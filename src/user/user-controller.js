const userService = require('./user-service');
const responseService = require('../../services/response-service');


module.exports.createUser = async (request, response) => {

    console.log(request.body);

    let result = await userService.createUser(request.body);


    if (result == false) {
        responseService.errorWithMessage(response, "Error Creating User");
    } else if (result == null) {
        responseService.errorWithMessage(response, "User already exist");
    } else {
        responseService.successWithData(response, "User successfully created");
    }

}

module.exports.loginUser = async (request, response) => {

    console.log(request.body);

    let result = await userService.loginUser(request.body);


    if (result == false) {

        responseService.errorWithMessage(response, "Error User Login");

    } else if (result == null) {

        responseService.errorWithMessage(response, "Please check your credintials...");
    } else {

        responseService.successWithToken(response, result);
    }

}



module.exports.getUsers = async (request, response) => {

    
    let myData = await userService.findUsers(request.query);

    if (myData == false) {
        responseService.errorWithMessage(response, "Error getting data");

    } else {
        responseService.successWithData(response, myData);
    }

}