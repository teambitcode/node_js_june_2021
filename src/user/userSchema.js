const Joi = require('joi');
const responseService = require('../../services/response-service');

var userCreateSchema = Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required()
});

var userUpdateSchema = Joi.object().keys({
    first_name: Joi.string(),
    last_name: Joi.string(),
    email: Joi.string().email(),
    password: Joi.string()
});


module.exports.validatePostBody = function name(req, res, next) {
    console.log("In validate fn");

    var result = userCreateSchema.validate(req.body);

    if (result.error) {
        responseService.errorWithMessage(res, result.error.message);
    } else {
        next();
    }
}

module.exports.validatePatchBody = function name(req, res, next) {
    console.log("In validate fn");

    var result = userUpdateSchema.validate(req.body);

    if (result.error) {
        responseService.errorWithMessage(res, result.error.message);
    } else {
        next();
    }
}