const express = require('express');
const mongoose = require('mongoose');

const sever = express();

const routerObject = require('./routes/router');
const serverConfigs = require('./configs/server-configs');

mongoose.connect(serverConfigs.msqlConnectionString, { useNewUrlParser: true });


sever.use(express.json());
sever.use(routerObject);

sever.listen(serverConfigs.port, function check(error) {
    if (error) {
        console.log("Error...!!");
    } else {
        console.log("Server stated....!");
    }
});